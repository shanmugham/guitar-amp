#include "fixed.h"

#include <stdlib.h>
#include "ST7735.h"
#include "util.h"

int32_t _minX, _maxX, _minY, _maxY;
char* _title;

int32_t _screenMinX = 0;
int32_t _screenMaxX = 127;
int32_t _screenMinY = 32;
int32_t _screenMaxY = 159;

int32_t _cursorBufferX = 4;
int32_t _cursorBufferY = 4;

/**
 * Output integer with three decimal places
 * @param n the integer to ouput as a string
 */
void ST7735_sDecOut3(int32_t n) {
	uint32_t size = 7;
	uint32_t decimalPart = 3;

	char* outString = (char*) malloc(sizeof(char) * size);

	if (abs(n) <= 9999) {
		IntToString(n, outString, size, decimalPart);
		if(n < 0){
			outString[0] = '-';
		}
		else{
			outString[0] = ' ';
		}
	} else {
		outString = " *.***";
	}

	ST7735_SetTextColor(ST7735_WHITE);
	ST7735_SetCursor(_cursorBufferX, _cursorBufferY);
	ST7735_OutString(outString);
}

/**
 * Output binary number as string
 * @param n integer to output
 */
void ST7735_uBinOut8(uint32_t n) {
	uint32_t size = 7;
	uint32_t decimalPart = 2;

	char* outString = (char*) malloc(sizeof(char) * size);

	if (abs(n) < 256000) {
		uint32_t k = n*1000;
		k /= 256;
		
		// round up or down as necessary
		if(k % 10 >= 5){
			k /= 10;
		}
		else{
			k /= 10;
		}

		IntToString(k, outString, size, decimalPart);
	} else {
		outString = "***.**";
	}

	ST7735_SetTextColor(ST7735_WHITE);
	ST7735_SetCursor(_cursorBufferX, _cursorBufferY);
	ST7735_OutString(outString);
}

/**
 * Initialize graphics area with a title and dimensions.
 * 
 * @param title the title of the plot
 * @param minX the min x dimension
 * @param maxX the max x dimesntion
 * @param minY the min y dimension
 * @param maxY the max y dimension
 */
void ST7735_XYplotInit(char *title, int32_t minX, int32_t maxX, int32_t minY, int32_t maxY) {
	_title = title;
	_minX = minX;
	_maxX = maxX;
	_minY = minY;
	_maxY = maxY;

	Output_Clear();


//	ST7735_SetTextColor(ST7735_WHITE);
//	ST7735_SetCursor(_cursorBufferX, _cursorBufferY);
//	ST7735_StringAtPixel(0,0,title, ST7735_WHITE, 2);
//	ST7735_OutString(title);
}

/**
 * Plot a list of x,y point pairs
 * @param num the number of point pairs
 * @param bufX pointer to list of x values
 * @param bufY pointer to list of y values
 */
void ST7735_XYplot(uint32_t num, int32_t bufX[], int32_t bufY[]) {
	uint32_t i;
	uint16_t x, y;

	for(i = 0; i < num; i++){
		if(bufX[i] < _minX || bufX[i] > _maxX
			|| bufY[i] < _minY || bufY[i] > _maxY){
			continue;
		}

		x = _screenMinX + ((bufX[i] - _minX) * (_screenMaxX - _screenMinX)) / (_maxX - _minX);
		y = _screenMinY + ((_maxY - bufY[i]) * (_screenMaxY - _screenMinY)) / (_maxY - _minY);

		ST7735_DrawPixel(x, y, ST7735_WHITE);
		ST7735_DrawFastVLine(x, y, 160-y, ST7735_CYAN);
	}
}

/**
 * Plot a list of x,y point pairs
 * @param num the number of point pairs
 * @param bufX pointer to list of x values
 * @param bufY pointer to list of y values
 */
void ST7735_XYplotWithColor(uint32_t num, int32_t bufX[], int32_t bufY[], uint32_t color) {
	uint32_t i;
	uint16_t x, y;

	for(i = 0; i < num; i++){
		if(bufX[i] < _minX || bufX[i] > _maxX
			|| bufY[i] < _minY || bufY[i] > _maxY){
			continue;
		}

		x = _screenMinX + ((bufX[i] - _minX) * (_screenMaxX - _screenMinX)) / (_maxX - _minX);
		y = _screenMinY + ((_maxY - bufY[i]) * (_screenMaxY - _screenMinY)) / (_maxY - _minY);

		// ST7735_DrawPixel(x, y, color);
		ST7735_DrawFastVLine(x, y, 160-y, color);
	}
}
