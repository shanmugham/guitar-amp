#include "util.h"
	
char* IntToString(int32_t n, char* string, int32_t size, int32_t dec)  {
	
	int32_t k = n;	
	int32_t c = size;
	
	c = c - 2;
	
	if(n < 0){
		k = abs(k);
	}

	while(c >= 0){
		if(k == 0){
			if(size - c > dec + 3){
				string[c] = ' ';
				c--;
				continue;
			}
		}
		if(size - c == dec + 2){
			string[c] = '.';
			c--;
			continue;
		}
		string[c] = k%10;
		string[c] += 48;
		k /= 10;
		c--;
	}
	
	c = size - 1;
	string[c] = '\0';
	
	return string;
}

int64_t* computeDifferences(uint32_t* arr, uint64_t size, int64_t* ret) {
	int64_t i;
	
	ret[0] = 0;
	
	for (i = 1; i < size; i += 1) {
		ret[i] = arr[i] - arr[i-1];
	}
	
	return ret;
}

int64_t maxArrayValue(int64_t* arr, int64_t size) {
	int64_t i;
	int64_t max;

	max = arr[0];
	
	for (i = 1; i < size; i += 1) {
		if (arr[i] > max) {
			max = arr[i];
		}
	}
	
	return max;
}

int64_t minArrayValue(int64_t* arr, int64_t size) {
	int64_t i;
	int64_t min;
	
	min = arr[0];
	
	for (i = 1; i < size; i += 1) {
		if (arr[i] < min) {
			min = arr[i];
		}
	}
	
	return min;
}
