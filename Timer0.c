// Timer0A.c
// Runs on LM4F120/TM4C123
// Use Timer0A in periodic mode to request interrupts at a particular
// period.
// Daniel Valvano
// September 11, 2013

/* This example accompanies the book
   "Embedded Systems: Introduction to ARM Cortex M Microcontrollers"
   ISBN: 978-1469998749, Jonathan Valvano, copyright (c) 2015
   Volume 1, Program 9.8

  "Embedded Systems: Real Time Interfacing to ARM Cortex M Microcontrollers",
   ISBN: 978-1463590154, Jonathan Valvano, copyright (c) 2014
   Volume 2, Program 7.5, example 7.6

 Copyright 2015 by Jonathan W. Valvano, valvano@mail.utexas.edu
    You may use, edit, run or distribute this file
    as long as the above copyright notice remains
 THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 VALVANO SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/
 */
#include <stdint.h>
#include <math.h>
#include "../inc/tm4c123gh6pm.h"
#include "ADCSWTrigger.h"
#include "Timer1.h"
#include "dac.h"
#include "FFT.h"
#include "globals.h"

void DisableInterrupts(void); // Disable interrupts
void EnableInterrupts(void);  // Enable interrupts
long StartCritical (void);    // previous I bit, disable interrupts
void EndCritical(long sr);    // restore I bit to previous value
void WaitForInterrupt(void);  // low power mode
void (*PeriodicTask)(void);   // user function


typedef struct {
  int16_t real,imag;
} Complex_t;

// input and output arrays for FFT
// two 16-bit signed numbers are packed into each 32-bit entry
// bits31-16 are imaginary part, signed 16 bits
// bits15-0 are real part, signed 16 bits
// for the input, normally x[] bits31-16 are cleared to mean real input
//                         x[] bits15-0 contain the input signal
// for the output,         y[] bits31-16 contain the imaginary part
//                         y[] bits15-0 contain the real part
//*********Prototype for FFT in cr4_fft_1024_stm32.s, STMicroelectronics
void cr4_fft_1024_stm32(Complex_t *pssOUT, Complex_t *pssIN, unsigned short Nbin);
void cr4_fft_256_stm32(Complex_t *pssOUT, Complex_t *pssIN, unsigned short Nbin);
void cr4_fft_64_stm32(Complex_t *pssOUT, Complex_t *pssIN, unsigned short Nbin);

// Newton's method
// s is an integer
// Sqrt(s) is an integer
uint32_t Sqrt(uint32_t s){
uint32_t t;   // t*t will become s
int n;             // loop counter
  t = s/16+1;      // initial guess
  for(n = 20; n; --n){ // will finish
    t = ((t*t+s)/t)/2;
  }
  return t;
}


// ***************** Timer0A_Init ****************
// Activate TIMER0 interrupts to run user task periodically
// Inputs:  task is a pointer to a user function
//          period in units (1/clockfreq), 32 bits
// Outputs: none
void Timer0A_Init(void(*task)(void), uint32_t period) {
	long sr;
  sr = StartCritical(); 
  SYSCTL_RCGCTIMER_R |= 0x01;   // 0) activate TIMER0
  PeriodicTask = task;          // user function
  TIMER0_CTL_R = 0x00000000;    // 1) disable TIMER0A during setup
  TIMER0_CFG_R = 0x00000000;    // 2) configure for 32-bit mode
  TIMER0_TAMR_R = 0x00000002;   // 3) configure for periodic mode, default down-count settings
  TIMER0_TAILR_R = period-1;    // 4) reload value
  TIMER0_TAPR_R = 0;            // 5) bus clock resolution
  
	TIMER0_ICR_R = 0x00000001;    // 6) clear TIMER0A timeout flag
  TIMER0_IMR_R = 0x00000001;    // 7) arm timeout interrupt
  NVIC_PRI4_R = (NVIC_PRI4_R&0x00FFFFFF)|0x20000000; // 8) priority 1
  // interrupts enabled in the main program after all devices initialized
  // vector number 35, interrupt number 19
  NVIC_EN0_R |= 1<<19;           // 9) enable IRQ 19 in NVIC
  
	TIMER0_CTL_R = 0x00000001;    // 10) enable TIMER0A
  EndCritical(sr);
}

// data for FFT
Complex_t x[1024];
Complex_t y[1024];

int32_t asm_fft_data_index = 0;
int32_t asm_fft_data_size = 1024;
int32_t mag[512];

void find_max_freq(){
	int size = global_frequency_array_size;
	int32_t max[6] = {0,0,0,0,0,0};//indices of max; last is biggest
	mag[0] = 0;
	for(int i = 4; i < 120; i ++){// 4-120 is hardcoded because those are the only freq.s that are significant on a guitar
		
		if(mag[i] < mag[max[0]]) continue;
		if(mag[i] - mag[i-1] < 5 || mag[i] - mag[i+1] < 5) continue;//if its not a maxima by 15.//tried with 2*i but was buggy
		
		for(int j = 1; j < size; j++){
			
			if(mag[i]+mag[i-1]+mag[i+1]+mag[i-2]+mag[i+2] 
				<= mag[max[j]]+mag[(int)fmax(max[j]-1, 0)]+mag[max[j]+1]+mag[(int)fmax(max[j]-2, 0)]+mag[max[j]+2]
				|| j == size - 1){
					
				// disregard harmonics
				int a = 0;
				for(int k = 1; k < size; k++){
					if(i == max[k]*2 ||
						i == (max[k]*2)+1 ||
						i == (max[k]*2)+1){
						a = 1;
						break;
					}
				}
				if(a) break;
							
				// if largest so far; larger than anything in max[]
				if(j == size - 1 && mag[i]+mag[i-1]+mag[i+1] > mag[max[j]]+mag[max[j]-1]+mag[max[j]+1]) j = size;
				
				//move everything over
				for(int k = 1; k < j; k++){
					max[k-1] = max[k];
				}
				max[j-1] = i;
				
				break;
			}
		}
	}
	
	for(int i = 0; i < 6; i++){
		global_frequency_array[i] = max[i]*10;
		
		// higher precision for in-between frequencies
//		if(mag[max[i]] - mag[max[i-1]] <= 50){
//			global_frequency_array[i] -= 10;
//		}
//		if(mag[max[i]] - mag[max[i+1]] <= 50){
//			global_frequency_array[i] += 10;
//		}
	}
}

void asm_fft_handler(void) {
	// If not full, collect value
	// Else, compute the FFT, and reset index so that we can start over again

		cr4_fft_1024_stm32(y, x, asm_fft_data_size);
		for(int k=0; k<512; k=k+1){         // k means fs/1024
			int real = y[k].real;             // bottom 16 bits
			int imag = y[k].imag;             // top 16 bits
			mag[k] = Sqrt(real*real+imag*imag);
		}
		find_max_freq();
		asm_fft_data_index = 0;
}

volatile uint8_t count = 0;

void Timer0A_Handler(void) {
	ADCFlag = 1;
	ADCValue = ADC0_InSeq3();
	TIMER0_ICR_R = TIMER_ICR_TATOCINT; // acknowledge timer0A timeout
	if (asm_fft_data_index < asm_fft_data_size) {
			x[asm_fft_data_index].real = ADCValue;
			x[asm_fft_data_index].imag = 0;
			asm_fft_data_index += 1;
	} else {
		SamplingFlag = 1;
	}
	
}
