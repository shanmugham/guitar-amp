#include "SysTick.h"
#include "DAC.h"
#include <math.h>
#include "globals.h"

#define M_PI 3.141592653

uint32_t SysTickDumpSize = 100;
float SysTickDump[100];
uint16_t SysTickDumpDAC[100];
uint32_t SysTickDumpIndex = 0;
uint32_t SysTickDumpDACIndex = 0;

void SysTick_Init(unsigned long period) {
	NVIC_ST_CTRL_R = 0;         // disable SysTick during setup
  NVIC_ST_RELOAD_R = period-1;// reload value
  NVIC_ST_CURRENT_R = 0;      // any write to current clears it
  NVIC_SYS_PRI3_R = (NVIC_SYS_PRI3_R&0x00FFFFFF)|0x00000000; // priority 0
  NVIC_ST_CTRL_R = 0x07; // enable SysTick with core clock and interrupts
  // enable interrupts after all initialization is finished
}

void SysTick_Init_WithoutInterrupts(void){
  NVIC_ST_CTRL_R = 0;                   // disable SysTick during setup
  NVIC_ST_RELOAD_R = NVIC_ST_RELOAD_M;  // maximum reload value
  NVIC_ST_CURRENT_R = 0;                // any write to current clears it
  NVIC_ST_CTRL_R = NVIC_ST_CTRL_ENABLE+NVIC_ST_CTRL_CLK_SRC; // enable SysTick with core clock
}

// These variables are global to make
// inspecting in the watch window easier
float seconds;
float freq;
float SinValueArg;
float SinValue;
float output;
int16_t dac_output;

void sin_wave_handler(void) {
	global_time += 99;
	if (global_time >= 1000000) {
		global_time = 0;
	}
	
	if(Bypass == 1){
		DAC_Out(ADCValue/2);
		return;
	}
	if(Bypass == 2){
		DAC_Out(ADCValue);
		return;
	}
	
	output = 0;
	
	for (uint32_t i = 5; i < 6; i += 1) {
		seconds = (float)global_time/1000000;
		freq = global_frequency_array[i];
		int a = 0x01<<(Octave);
		freq = freq*a;
		freq = freq/2;
		
		SinValueArg =  (2*M_PI*freq*seconds);
		SinValue = sin(SinValueArg);
		output += SinValue; // sin(2*pi*f*t) wave repeat every 1 second
	}
	
	output *= 340*6; // (2^11)/6-1 so the output range is [-2040,2040]
	output += 2048;
	
	dac_output = output/2; // convert 12-bit data size -> 11-bit data size
	
  //  Commented out debugging dump
  /* 
	if (SysTickDumpIndex < SysTickDumpSize) {
			SysTickDump[SysTickDumpIndex] = output;
			SysTickDumpDAC[SysTickDumpDACIndex] = (uint16_t) dac_output;
			SysTickDumpIndex += 1;
  }
	*/
	
		DAC_Out((uint16_t) dac_output);

}

// Interrupt service routine
// Executed periodically, the actual period
// determined by the current Reload.
void SysTick_Handler(void){
		// no acknowledge for SysTick interrupts!
		sin_wave_handler();
		//SysTickFlag = 1;
}

// Time delay using busy wait.
// The delay parameter is in units of the core clock. (units of 20 nsec for 50 MHz clock)
// INFO:nshanmugham: 
// Comment in paranthesis about 20 nsec units for 50 MHz clock were made by Dr. Valvano.
// Do NOT trust at face-value.
void SysTick_Wait(uint32_t delay){
  volatile uint32_t elapsedTime;
  uint32_t startTime = NVIC_ST_CURRENT_R;
  do{
    elapsedTime = (startTime-NVIC_ST_CURRENT_R)&0x00FFFFFF;
  }
  while(elapsedTime <= delay);
}

