#include "util.h"
#include "stdlib.h"

void Timer1_Init(void);
void Timer1A_Handler(void);
uint32_t Timer1A_GetTimeElapsed(void);
void Timer1A_Reset(void);
