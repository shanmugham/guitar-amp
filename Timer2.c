#include "Timer2.h"
#include "../inc/tm4c123gh6pm.h"
#include "globals.h"
#include "ST7735.h"

long StartCritical (void);    // previous I bit, disable interrupts
void EndCritical(long sr);    // restore I bit to previous value
void DisableInterrupts(void); // Disable interrupts
void EnableInterrupts(void);  // Enable interrupts

void Timer2_Init(unsigned long reload) {   
		volatile uint32_t delay;   		
		SYSCTL_RCGCTIMER_R |= 0x04;	  // 0) activate TIMER2 
		delay = SYSCTL_RCGCTIMER_R;   // allow time to finish activating   
		TIMER2_CTL_R = 0x00000000;    // 1) disable TIMER2A during setup   
		TIMER2_CFG_R = 0x00000000;    // 2) configure for 32-bit mode   
		TIMER2_TAMR_R = 0x00000002;   // 3) configure for periodic mode, down-count   
		TIMER2_TAILR_R = reload - 1;  // 4) reload value   
		TIMER2_TAPR_R = 0;            // 5) bus clock resolution  
	
	  TIMER2_ICR_R = 0x00000001;    // 6) clear TIMER2A timeout flag
		TIMER2_IMR_R = 0x00000001;    // 7) arm timeout interrupt
	  NVIC_PRI5_R = (NVIC_PRI5_R&0x00FFFFFF)|0x00000000; // 8) priority TODO: may need to be higher if the handler does not happen
		
		// interrupts enabled in the main program after all devices initialized
		NVIC_EN0_R |= 1<<23;           // 9) enable IRQ 23 in NVIC
	
		TIMER2_CTL_R = 0x00000001;    // 10) enable
}

void Timer2A_Handler(void) {
	if (DebounceSwitchesFlag) {
		long sr = StartCritical();

		TIMER2_IMR_R = 0x00; // disable timer 2 interrupt from firing again
		TIMER2_ICR_R = TIMER_ICR_TATOCINT;// acknowledge TIMER2A timeout
		TIMER2_TAILR_R = DebounceSwitchesDelay; // reload value for Timer 2 firing
		DisableInterrupts();
		uint32_t dataRegister = ~GPIO_PORTE_DATA_R & 0x7;
		uint32_t data = (GPIO_PORTE_RIS_R) & (0x07); // PE0-2 negative logic
		EnableInterrupts();
		
		GPIO_PORTE_ICR_R = 0x07; // acknowledge

		//ST7735_SetCursor(1, 4);
		//ST7735_OutUDec(TIMER2_TAR_R);

		if(dataRegister == 0x0)	{ // nothing we want was pressed
			EndCritical(sr);
			GPIO_PORTE_IM_R |= 0x07; // PE0-2 arm interrupt
			return;
		}
		
		EndCritical(sr);

		uint32_t shiftsCount = 0;
		while ((data % 2) == 0) {
			data = data >> 1;
			shiftsCount += 1;
		}
		
		if (shiftsCount == 0) {
			Bypass++;
			Bypass %= 3;
			if(Bypass == 1){
				ST7735_StringAtPixel(100,5,"   ", ST7735_WHITE, 2);
				ST7735_StringAtPixel(4,5,"  Bypass", ST7735_WHITE, 2);
				ST7735_StringAtPixel(46,25,"   ", ST7735_WHITE, 2);
			} else if (Bypass == 2) {
				ST7735_StringAtPixel(100,5,"   ", ST7735_WHITE, 2);
				ST7735_StringAtPixel(4,5,"   Gain   ", ST7735_WHITE, 2);
				ST7735_StringAtPixel(46,25,"   ", ST7735_WHITE, 2);
			} else if (Bypass == 0) {
				ST7735_StringAtPixel(100,5,"   ", ST7735_WHITE, 2);
				ST7735_StringAtPixel(0,5,"  Octaver", ST7735_WHITE, 2);
				if(Octave == 0) ST7735_StringAtPixel(52,25,"-1", ST7735_WHITE, 2);
				else if(Octave == 1) ST7735_StringAtPixel(46,25," 0 ", ST7735_WHITE, 2);
				else if(Octave == 2) ST7735_StringAtPixel(46,25," 1 ", ST7735_WHITE, 2);
			}
		} else if (shiftsCount == 1 && !Bypass) {
			Octave = Octave >= 2 ? 2 : (Octave + 1);
			if(Octave == 2) ST7735_StringAtPixel(46,25," 1 ", ST7735_WHITE, 2);
			else if(Octave == 1) ST7735_StringAtPixel(46,25," 0 ", ST7735_WHITE, 2);
		} else if (shiftsCount == 2 && !Bypass) {
			Octave = Octave <= 0 ? 0 : (Octave - 1);
			if(Octave == 0) ST7735_StringAtPixel(52,25,"-1", ST7735_WHITE, 2);
			else if(Octave == 1) ST7735_StringAtPixel(46,25," 0 ", ST7735_WHITE, 2);
		}
		
		GPIO_PORTE_IM_R |= 0x07; // PE0-2 arm interrupt
	}
}
