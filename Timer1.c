#include "Timer1.h"
#include "../inc/tm4c123gh6pm.h"

// ***************** TIMER1_Init ****************
// Activate TIMER1 interrupts to run user task periodically
// Inputs:  task is a pointer to a user function
//          period in units (1/clockfreq)
// Outputs: none
void Timer1_Init() {   
		volatile uint32_t delay;   		
		SYSCTL_RCGCTIMER_R |= 0x02;	  // 0) activate TIMER1   
		delay = SYSCTL_RCGCTIMER_R;   // allow time to finish activating   
		TIMER1_CTL_R = 0x00000000;    // 1) disable TIMER1A during setup   
		TIMER1_CFG_R = 0x00000000;    // 2) configure for 32-bit mode   
		TIMER1_TAMR_R = 0x00000002;   // 3) configure for periodic mode, down-count   
		TIMER1_TAILR_R = 0xFFFFFFFF - 1;  // 4) reload value   
		TIMER1_TAPR_R = 0;            // 5) bus clock resolution  
	
		TIMER1_ICR_R = 0x00000001;    // 6) clear TIMER2A timeout flag
		TIMER1_IMR_R = 0x00000001;    // 7) arm timeout interrupt
		NVIC_PRI5_R = (NVIC_PRI5_R&0x00FFFFFF)|0x02000; // 8)
		
		// interrupts enabled in the main program after all devices initialized
		NVIC_EN0_R |= 1<<21;           // 9) enable IRQ 21 in NVIC
		
		TIMER1_CTL_R = 0x00000001;    // 10) enable
}
	
void Timer1A_Handler(void) {
  TIMER1_ICR_R = TIMER_ICR_TATOCINT; // acknowledge TIMER1A timeout
}

uint32_t Timer1A_GetTimeElapsed(void) {
	return TIMER1_TAILR_R - TIMER1_TAR_R;
}

void Timer1A_Reset() {
	TIMER1_TAILR_R = 0xFFFFFFFF - 1;
	TIMER1_TAR_R = TIMER1_TAILR_R;
}
