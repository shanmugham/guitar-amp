// PeriodicTimer0AInts.c
// Runs on LM4F120/TM4C123
// Use Timer0A in periodic mode to request interrupts at a particular
// period.
// Daniel Valvano
// September 11, 2013

/* This example accompanies the book
   "Embedded Systems: Real Time Interfacing to Arm Cortex M Microcontrollers",
   ISBN: 978-1463590154, Jonathan Valvano, copyright (c) 2015
  Program 7.5, example 7.6

 Copyright 2015 by Jonathan W. Valvano, valvano@mail.utexas.edu
    You may use, edit, run or distribute this file
    as long as the above copyright notice remains
 THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 VALVANO SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/
 */

// __Best Settings on Fender Mustang I amp:
// Volume: 10
// Gain: 10
// Master: 5
// All effects off

#include "../inc/tm4c123gh6pm.h"
#include <stdint.h>
#include "PLL.h"
#include "Timer0.h"
#include "Timer1.h"
#include "Timer2.h"
#include "SysTick.h"
#include "dac.h"
#include "switch.h"
#include "ADCSWTrigger.h"
#include "FFT.h"
#include "ST7735.h"
#include "fixed.h"
#include "globals.h"

// Was used to toggle port F LEDs
// In Lab 11, Port F is needed by the DAC
#define PF1       (*((volatile uint32_t *)0x40025008))
#define PF2       (*((volatile uint32_t *)0x40025010))
#define PF3       (*((volatile uint32_t *)0x40025020))
#define LEDS      (*((volatile uint32_t *)0x40025038))
#define RED       0x02
#define BLUE      0x04
#define GREEN     0x08
#define WHEELSIZE 8  // must be an integer multiple of 2
// red, yellow, green, light blue, blue, purple, white, dark
const long COLORWHEEL[WHEELSIZE] = {RED, RED+GREEN, GREEN, GREEN+BLUE, BLUE, BLUE+RED, RED+GREEN+BLUE, 0};

void DisableInterrupts(void); // Disable interrupts
void EnableInterrupts(void);  // Enable interrupts
long StartCritical (void);    // previous I bit, disable interrupts
void EndCritical(long sr);    // restore I bit to previous value
void WaitForInterrupt(void);  // low power mode

void timer_0_user_task(void) {
	// (empty)
}

// The defines and comments below are from Lab 5
// If the desired interrupt frequency is f, 
// Timer0A_Init parameter is busfrequency/f
#define F16HZ (50000000/16)
#define F20KHZ (50000000/20000)

int32_t TOTAL_SAMPLES = 128;
int32_t xcoordinates[128];		
int32_t ycoordinates[128];
int32_t oldMag[128];

int main(void) { 
	// Start-up essentials
  PLL_Init(Bus80MHz); // bus clock at 80 MHz (see PLL.h for more options)
	
	// Disable interrupts before we being initializations
	DisableInterrupts();
	
	// variables used for initialization
	uint32_t systick_period = 8000; // 10kHz
	uint32_t timer0_period = 8000; // 10kHz (80Mhz/10khz)
	
	// Initialization
	ADC0_InitSWTriggerSeq3_Ch7();		
	DAC_Init(0x1000); // initialize with command: Vout = Vref (copied for MAX53535 project)	
	Switches_Init();
	SysTick_Init(systick_period);
	Timer0A_Init(&timer_0_user_task, timer0_period);
	Timer2_Init(DebounceSwitchesDelay);
	TIMER2_IMR_R = 0x00; // this is fine. we can unarm interrupt after full init. handler will not run because `EnableInterrupts()` hasn't been called yet.
	
	// fill up x coordinates array once
	for (uint32_t i = 0; i < TOTAL_SAMPLES; i +=1 ) {
		xcoordinates[i] = i;
	}
	
	for (uint32_t i = 0; i < TOTAL_SAMPLES; i +=1 ) {
		ycoordinates[i] = 0;
	}
	
	// LCD (
	ST7735_InitR(INITR_REDTAB);
	ST7735_FillScreen(0);
	//ST7735_XYplotInit("Input", 0, 127, 2048, 8000);
	ST7735_XYplotInit("Input", 0, 127, 0, 400);
	ST7735_StringAtPixel(22,5,"Octaver", ST7735_WHITE, 2);
	ST7735_StringAtPixel(61,25,"0", ST7735_WHITE, 2);
	
	// Enable interrupts after all initializations are done
	EnableInterrupts();
  
	while(1) {
		if(SysTickFlag){
			SysTickFlag = 0;
//			long sr = StartCritical();
//			sin_wave_handler();
//			EndCritical(sr);
		}
		if (ADCFlag == 1) {
			ADCFlag = 0; // clear flag
			
			
			// clear screen
//			ST7735_XYplotWithColor(128, xcoordinates, ycoordinates, ST7735_BLACK);
//			
//			for (uint32_t i = 0; i < TOTAL_SAMPLES-1; i +=1 ) {
//				ycoordinates[i] = ycoordinates[i+1];
//			}
//		
//			ycoordinates[TOTAL_SAMPLES - 1] = ADCValue;

//			ST7735_XYplotWithColor(128, xcoordinates, ycoordinates, ST7735_CYAN);
		}
		if (SamplingFlag == 1){
			SamplingFlag = 0;
			//if(!Bypass)
				asm_fft_handler();
			
			// clear screen
			ST7735_XYplotWithColor(128, xcoordinates, oldMag, ST7735_BLACK);
			
			ST7735_XYplotWithColor(128, xcoordinates, mag, ST7735_CYAN);
			
			for(int i = 0; i < 128; i ++){
				oldMag[i] = mag[i];
			}
		}
	}
}


