# Lab 11

Guitar amp + effects + other cool stuff

* Rahul Jaisimha
* Nishanth Shanmugham

### Which uvproj ###

* Use the uvproj from the project `PeriodicTimer0AInts_4C123`

### What files were in the uvproj in the initial commit that Nishanth made?

* See this image: http://imgur.com/Ij3PW6I

### Main file?

* `PeriodicTimer0AInts.c`

### Things to watch out for

* Make sure the project folder is placed relative to `../inc/tm4c123gh6pm.h`
* Some inconsistent naming of variables (uppercase, underscores)
* Make sure to look at `globals.c` and `globals.h` file
* Double-checking to make sure the timer you want is enabled

### Links

* [Valvano FFT assembly files page](http://users.ece.utexas.edu/~valvano/arm/#RTOS)