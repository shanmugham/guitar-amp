#include "globals.h"

uint32_t global_time = 0;
int32_t global_frequency_array_size = 6;
uint32_t global_frequency_array[6];
int32_t ADCFlag = 0;
int32_t ADCValue;
int32_t SamplingFlag = 0;
int32_t SysTickFlag = 0;
uint32_t Octave = 1;
uint32_t Bypass = 0;
int32_t DebounceSwitchesFlag = 1;
uint32_t DebounceSwitchesDelay = 40000000; // 500ms; 10ms => 800,000
