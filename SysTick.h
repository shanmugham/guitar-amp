#include "util.h"
#include <stdint.h>
#include <stdlib.h>
#include "../inc/tm4c123gh6pm.h"

void SysTick_Init(unsigned long period);
void SysTick_Init_WithoutInterrupts(void);
void SysTick_Wait(uint32_t delay);

void sin_wave_handler(void);
