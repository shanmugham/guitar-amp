#include "switch.h"
#include "../inc/tm4c123gh6pm.h"
#include "globals.h"
#include "ST7735.h"

void DisableInterrupts(void); // Disable interrupts
void EnableInterrupts(void);  // Enable interrupts
void WaitForInterrupt(void);  // low power mode

long StartCritical (void);    // previous I bit, disable interrupts
void EndCritical(long sr);    // restore I bit to previous value

// PORT E base is 0x40024000
// Reference: http://users.ece.utexas.edu/~valvano/Volume1/E-Book/C6_MicrocontrollerPorts.htm
#define PE5 (*((volatile unsigned long *)0x40024080))
#define PE4 (*((volatile unsigned long *)0x40024040))
#define PE3 (*((volatile unsigned long *)0x40024020))
#define PE2 (*((volatile unsigned long *)0x40024010))
#define PE1 (*((volatile unsigned long *)0x40024008))
#define PE0 (*((volatile unsigned long *)0x40024004))

volatile unsigned long FallingEdges = 0;

const int32_t PORT_E_CLOCK = 0x010;
const int32_t PE02_BIT_PATTERN = 0x07;

void Switches_Init(void) {

	SYSCTL_RCGCGPIO_R |= PORT_E_CLOCK; // enable clock
	while ((SYSCTL_PRGPIO_R & PORT_E_CLOCK) == 0) {} // wait until clock starts
	// Looks like there is no need to unlock PORT E
	// GPIO_PORTE_LOCK_R
	GPIO_PORTE_AMSEL_R &= ~PE02_BIT_PATTERN; // disable analog 
	GPIO_PORTE_PCTL_R &= ~PE02_BIT_PATTERN; // disable alt. functions and use as GPIO
	GPIO_PORTE_DIR_R &= ~PE02_BIT_PATTERN; // all inputs
	GPIO_PORTE_AFSEL_R &= ~PE02_BIT_PATTERN; // no alt. function to select
	GPIO_PORTE_PUR_R |= PE02_BIT_PATTERN; // pull up resistor on PE2-0
	GPIO_PORTE_IS_R &= ~PE02_BIT_PATTERN;     // (d) PE2-0 is edge-sensitive
	GPIO_PORTE_IBE_R &= ~PE02_BIT_PATTERN;    //     PE2-0 is not both edges
  GPIO_PORTE_IEV_R &= ~PE02_BIT_PATTERN;    //     PE2-0 falling edge event
	GPIO_PORTE_ICR_R = PE02_BIT_PATTERN;      // (e) clear flag (does not need to be safe write)
  GPIO_PORTE_IM_R |= PE02_BIT_PATTERN;      // (f) arm interrupt on PE2-0
	GPIO_PORTE_DEN_R |= PE02_BIT_PATTERN; // enable digital of PE2-0
	NVIC_PRI1_R = (NVIC_PRI1_R&0xFFFFFF00) | 0x00000000; // priority 0 (table 12.2 http://users.ece.utexas.edu/~valvano/Volume1/E-Book/C12_Interrupts.htm)
	NVIC_EN0_R |= 0x00000010; // enable interrupt 4 in NVIC
}

void GPIOPortE_Handler(void) {
	
	if (DebounceSwitchesFlag) {
		// - to debounce, we disable switch interrupts and re-enable after `DebounceSwitchesDelay`. 
		// - the disabling of interrupt happens in this block.
		// - reenabling happens in use a Timer that fires after the required duration.
		GPIO_PORTE_IM_R &= ~PE02_BIT_PATTERN; // unarm interrupts on PE2-0
		
		TIMER2_IMR_R = 0x00; // disable timer2 before setting reload value
		TIMER2_TAILR_R = DebounceSwitchesDelay; // reload value for Timer 2 firing
		TIMER2_IMR_R = 0x01; // enable timer 2
	}
	
	
}
