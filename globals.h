#include "util.h"
#include <stdlib.h>
#include <stdint.h>

extern uint32_t global_time;
extern int32_t global_frequency_array_size;
extern uint32_t global_frequency_array[6];
extern int32_t ADCFlag;
extern int32_t ADCValue;
extern int32_t SamplingFlag;
extern int32_t SysTickFlag;
extern uint32_t Octave;// 0 = lower octave
											 // 1 = normal octave
											 // 2 = higher octave
extern uint32_t Bypass;// 0 = Octaver
											 // 1 = Bypass
											 // 2 = Gain

extern int32_t DebounceSwitchesFlag;
extern uint32_t DebounceSwitchesDelay;
