#include <stdlib.h>
#include <stdint.h>

#define False 0
#define True 1

typedef int32_t Boolean;

/**************IntToString***************
 Plot an array of (x,y) data
 Inputs:  n		   number to change in integer form
          size	 size of string including null
          string preallocated string pointer
					dec		 number of decimal places. -1 if no decimal point needed
 Outputs: string
*/
char* IntToString(int32_t n, char* string, int32_t size, int32_t dec);

int64_t* computeDifferences(uint32_t* arr, uint64_t size, int64_t* ret);
int64_t maxArrayValue(int64_t* arr, int64_t size);
int64_t minArrayValue(int64_t* arr, int64_t size);
